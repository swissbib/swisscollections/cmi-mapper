package ch.swisscollections

import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.scala.Logging

import java.time.Duration
import scala.util.{Failure, Success, Try}

object App extends scala.App with AppSettings with Logging {

  val topology = new KafkaTopology

  val streams = new KafkaStreams(
    topology.build,
    kafkaStreamsSettings
  )

  val shutdownGracePeriodMs = 10000

  logger.trace("Starting stream processing")
  Try(
    streams.start()
  ) match {
    case Success(_) =>
      logger.info("Kafka Streams workflow successfully started.")
    case Failure(f) =>
      logger.error(s"Aborting due to errors: ${f.getMessage}")
      sys.exit(1)
  }


  sys.ShutdownHookThread {
    streams.close(Duration.ofMillis(shutdownGracePeriodMs))
  }

}
