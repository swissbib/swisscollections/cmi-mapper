package ch.swisscollections

import java.util.Properties
import scala.util.{Success, Try}

trait AppSettings {
  val bootstrapServers = sys.env("KAFKA_BOOTSTRAP_SERVERS")
  val applicationId = sys.env("APPLICATION_ID")
  val kafkaStreamsSettings = {
    val props = new Properties
    props.setProperty("application.id", applicationId)
    props.setProperty("bootstrap.servers", bootstrapServers)
    Try(sys.env("KAFKA_STREAMS_SETTINGS")) match {
      case Success(e) if e.nonEmpty =>
        e.split(",").foreach { v => {
          val elems = v.split('=')
          props.setProperty(elems(0), elems(1))
        }
        }
        props
      case _ => props
    }
  }
  val inputTopic = sys.env("TOPIC_IN")
  val outputTopic = sys.env("TOPIC_OUT")
  val deleteTopic = sys.env("TOPIC_DELETES")
}
