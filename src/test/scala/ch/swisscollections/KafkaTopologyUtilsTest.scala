/*
 * cmi-mapper
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections


import ch.swisscollections.KafkaTopologyUtils._
import org.scalatest.StreamlinedXml.streamlined
import org.scalactic.Explicitly.after
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}
import scala.io.Source
import scala.xml.{Elem, NodeSeq, XML}


class KafkaTopologyUtilsTest extends AnyFunSuite {

  def loadRecord(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/record/$filename")
    val record = file.mkString
    val recordAsXml = XML.loadString(record)
    recordAsXml
  }
  def loadResult(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/result/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }


  test("is from cmi") {
    assert(KafkaTopologyUtils.isFromCMI("(RZS)oai:alma.41SLSP_RZS:9914218356005505") == false)
    assert(KafkaTopologyUtils.isFromCMI("oai:helveticat.nb.admin.ch:991017986466003976") == false)
    assert(KafkaTopologyUtils.isFromCMI("(ZBCMI)oai:ZBcollections:a2a82ad8023c419faf379280eb34fc17") == true)
  }


  ignore("first mapping test") {
    val recordAsXml = loadRecord("cmi.xml")
    val mappedRecord = KafkaTopologyUtils.xmlMapper(recordAsXml)

    //should not do anything, but without this, the test fails
    val mappedRecordParsedAgain = XML.loadString(mappedRecord.toString())

    mappedRecordParsedAgain should equal (loadResult("result.xml")) (after being streamlined[Elem])

  }

  test("no callnumber") {
    val recordAsXml = loadRecord("no-callnumber.xml")
    val mappedRecord = KafkaTopologyUtils.xmlMapper(recordAsXml)
    println(mappedRecord)


  }

  test ("create field") {
    val recordAsXml = loadRecord("cmi.xml")
    val resultElem = getField(recordAsXml, "Format", "300##a").toString()
    XML.loadString(resultElem) should equal (loadResult("create-field-300.xml")) (after being streamlined[Elem])
  }

  test ("dates") {
    val recordAsXml = loadRecord("cmi.xml")
    val result = getDate(recordAsXml)
    assert(result == "q17601801")

    val datestamp = getDatestamp(recordAsXml)
    assert(datestamp.toString() == "<controlfield tag=\"005\">20230421120325.0</controlfield>")
  }

  test("contributors") {
    //mit mehreren 600, mit 100, mit 1 700
    val recordAsXML2 = loadRecord("person-subject.xml")
    val result2Main = getMainEntry(recordAsXML2).toString()
    val result2Subject = <dummy>{getSubjectEntry(recordAsXML2)}</dummy>.toString()
    val result2Added = <dummy>{getAddedEntry(recordAsXML2)}</dummy>.toString()

    XML.loadString(result2Main) should equal (loadResult("person-subject_main.xml")) (after being streamlined[Elem])
    XML.loadString(result2Subject) should equal (loadResult("person-subject_subject.xml")) (after being streamlined[Elem])
    XML.loadString(result2Added) should equal (loadResult("person-subject_added.xml")) (after being streamlined[Elem])

    //mit mehreren Beziehungskennzeichnungen für 100
    val recordAsXML3 = loadRecord("person-mains.xml")
    val result3Main = getMainEntry(recordAsXML3).toString()
    val result3Added = getAddedEntry(recordAsXML3).toString()

    XML.loadString(result3Main) should equal (loadResult("person-mains_main.xml")) (after being streamlined[Elem])
    XML.loadString(result3Added) should equal (loadResult("person-mains_added.xml")) (after being streamlined[Elem])

    //ohne 100, mit 1 600, mit mehreren 700, no GND
    val recordAsXML4 = loadRecord("person-no-main.xml")
    val result4Main: NodeSeq = getMainEntry(recordAsXML4)
    val result4Subject = getSubjectEntry(recordAsXML4).toString()
    val result4Added = <dummy>{getAddedEntry(recordAsXML4)}</dummy>.toString()

    assert(result4Main == NodeSeq.Empty)
    XML.loadString(result4Subject) should equal (loadResult("person-no-main_subject.xml")) (after being streamlined[Elem])
    XML.loadString(result4Added) should equal (loadResult("person-no-main_added.xml")) (after being streamlined[Elem])

  }

  test("series") {
    val recordAsXml = loadRecord("cmi.xml")
    val result = getParent(recordAsXml, "830").toString()

    XML.loadString(result) should equal (loadResult("series.xml")) (after being streamlined[Elem])

    val recordAsXml2 = loadRecord("bestand.xml")
    val result2 = getParent(recordAsXml2, "990").toString()
    XML.loadString(result2) should equal (loadResult("parent-unit.xml")) (after being streamlined[Elem])

    val recordAsXml3 = loadRecord("autograph.xml")
    val result3 = getParent(recordAsXml3, "830").toString()

    XML.loadString(result3) should equal(loadResult("series-autograph.xml"))(after being streamlined[Elem])
  }

  test("production notice") {
    val recordAsXml = loadRecord("places.xml")
    val result = getProductionNotice(recordAsXml).toString()

    XML.loadString(result) should equal (loadResult("productionNotice.xml")) (after being streamlined[Elem])
  }
  test("language") {
    val recordAsXml = loadRecord("languages.xml")
    val languageFirst = (recordAsXml \ "metadata" \ "Item" \ "Languages" \ "Language").head.text
    val result = getLanguages(recordAsXml).toString()

    assert(languageFirst == "ger")
    XML.loadString(result) should equal (loadResult("language.xml")) (after being streamlined[Elem])

    val recordAsXml2 = loadRecord("places.xml")
    val result2 = getLanguages(recordAsXml2)
    assert(result2 == NodeSeq.Empty)
  }

  test("topics") {
    val recordAsXml = loadRecord("cmi.xml")
    val result = <dummy>{getTopics(recordAsXml)}</dummy>.toString()

    XML.loadString(result) should equal (loadResult("topics.xml")) (after being streamlined[Elem])
  }

  test("formats") {
    val recordAsXml = loadRecord("person-subject.xml")
    val ldr = getFormat(recordAsXml)
    val matSpec = getMaterialSpecific(recordAsXml)
    val gndContent = <dummy>{getContent(recordAsXml)}</dummy>.toString()
    assert(ldr == "t")
    assert(matSpec == "||||| |||||||| ||")
    XML.loadString(gndContent) should equal (loadResult("gndcontent.xml")) (after being streamlined[Elem])

    val recordAsXml2 = loadRecord("no-callnumber.xml")
    val ldr2 = getFormat(recordAsXml2)
    val matSpec2 = getMaterialSpecific(recordAsXml2)
    assert(ldr2 == "k")
    assert(matSpec2 == "nnn |     |    ||")

    val recordAsXml3 = loadRecord("person-no-main.xml")
    val ldr3 = getFormat(recordAsXml3)
    val matSpec3 = getMaterialSpecific(recordAsXml3)
    assert(ldr3 == "t")
    assert(matSpec3 == "||||| |||||||| ||")

    val recordAsXml4 = loadRecord("foto.xml")
    val ldr4 = getFormat(recordAsXml4)
    val matSpec4 = getMaterialSpecific(recordAsXml4)
    assert(ldr4 == "g")
    assert(matSpec4 == "||| |     |    f|")
  }

  test("links") {
    val recordAsXml = loadRecord("link-doi.xml")
    val link = <dummy>{getLinkDigitized(recordAsXml)}</dummy>.toString()
    val doi = getDoi(recordAsXml).toString()

    XML.loadString(link) should equal(loadResult("link.xml"))(after being streamlined[Elem])
    XML.loadString(doi) should equal(loadResult("doi.xml"))(after being streamlined[Elem])

  }

  test ("remove url")
  {
    val recordAsXml = loadRecord("findingaid.xml")
    val result = {getField(recordAsXml, "FindingAid", "555##a")}.toString()

    XML.loadString(result) should equal(loadResult("remove-url.xml"))(after being streamlined[Elem])

    val recordAsXml2 = loadRecord("languages.xml")
    val result2 = {getField(recordAsXml2, "FindingAid", "555##a")}.toString()
    XML.loadString(result2) should equal(loadResult("remove-url-2.xml"))(after being streamlined[Elem])

  }

  test ("holding") {
    val recordAsXml = loadRecord("bestand.xml")
    val result = {getHolding(recordAsXml)}.toString()
    XML.loadString(result) should equal(loadResult("bestand-holding.xml"))(after being streamlined[Elem])

    val recordAsXml2 = loadRecord("places.xml")
    val result2 = {getHolding(recordAsXml2)}.toString()
    XML.loadString(result2) should equal(loadResult("places-holding.xml"))(after being streamlined[Elem])
  }

}
